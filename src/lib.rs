use itertools::Itertools;
use std::error::Error;
use std::fs;
use walkdir::{DirEntry, WalkDir};

#[derive(Debug)]
pub enum Operation<'a> {
    Rename(&'a str),
    Delete,
    None,
}

pub struct Config<'a> {
    pub path: &'a str,
    pub patterns: Vec<&'a str>,
    pub operation: Operation<'a>,
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let patterns: Vec<&str> = config.patterns.into_iter().unique().collect();
    let paths_that_match: Vec<DirEntry> = WalkDir::new(config.path)
        .into_iter()
        .filter_map(Result::ok)
        .filter(|entry| {
            entry.path().file_name().map_or(false, |path| {
                patterns
                    .iter()
                    .any(|pattern| path.to_str().unwrap_or("").contains(pattern))
            })
        })
        .collect();
    match config.operation {
        Operation::Rename(s) => rename(s, paths_that_match)?,
        Operation::Delete => delete(paths_that_match)?,
        Operation::None => print_matchs(paths_that_match)?,
    }
    Ok(())
}

fn rename(s: &str, entries: Vec<DirEntry>) -> Result<(), Box<dyn Error>> {
    let file_entries = entries.iter().filter(|entry| entry.path().is_file());
    for file in file_entries {
        let new_name = file.path().with_file_name(s);
        match fs::rename(file.path(), new_name.as_path()) {
            Err(e) => {
                eprintln!("Error Renaming {} {}", file.path().display(), e);
            }
            Ok(_) => {
                println!(
                    "Renaming {} to {}",
                    file.path().display(),
                    new_name.display()
                );
            }
        }
    }
    let dir_entries = entries.iter().filter(|entry| entry.path().is_dir());
    for dir in dir_entries {
        let new_name = dir.path().with_file_name(s);
        match fs::rename(dir.path(), new_name.as_path()) {
            Err(e) => {
                eprintln!("Error Renaming {} {}", dir.path().display(), e);
            }
            Ok(_) => {
                println!(
                    "Renaming {} to {}",
                    dir.path().display(),
                    new_name.display()
                );
            }
        }
    }
    Ok(())
}

fn delete(entries: Vec<DirEntry>) -> Result<(), Box<dyn Error>> {
    let dir_entries = entries.iter().filter(|entry| entry.path().is_dir());
    for dir in dir_entries {
        match fs::remove_dir_all(dir.path()) {
            Err(e) => {
                eprintln!("Error Deleting {} {}", dir.path().display(), e);
            }
            Ok(_) => {
                println!("Deleting {}", dir.path().display(),);
            }
        }
    }
    let file_entries = entries.iter().filter(|entry| entry.path().is_file());
    for file in file_entries {
        match fs::remove_file(file.path()) {
            Err(e) => {
                eprintln!("Error Deleting {} {}", file.path().display(), e);
            }
            Ok(_) => {
                println!("Deleting {}", file.path().display(),);
            }
        }
    }
    Ok(())
}
fn print_matchs(dir_entries: Vec<DirEntry>) -> Result<(), Box<dyn Error>> {
    for entry in dir_entries {
        println!("{}", entry.path().display());
    }
    Ok(())
}
