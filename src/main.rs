use std::process;

#[macro_use]
extern crate clap;
use clap::{App, AppSettings, Arg, SubCommand, Values};

use findx::Config;
use findx::Operation;

fn main() {
    let app = App::new("findx")
        .version(crate_version!())
        .author(crate_authors!())
        .about("find + directory operations utility")
        .setting(AppSettings::ArgRequiredElseHelp)
        .arg(
            Arg::with_name("path")
                .value_name("PATH")
                .takes_value(true)
                .help("Directory to search for the PATTERNS")
        )
        .arg(
            Arg::with_name("patterns")
                .short("p")
                .long("pattern")
                .value_name("PATTERNS")
                .required(true)
                .takes_value(true)
                .multiple(true)
                .number_of_values(1)
                .help("Patterns to search for"),
        )
        .subcommand(
            SubCommand::with_name("rename")
                .about("Rename the last part element of the path")
                .arg(
                    Arg::with_name("string")
                        .value_name("STRING")
                        .takes_value(true)
                        .required(true)
                        .help("Rename every dir/file founded"),
                ),
        )
        .subcommand(
            SubCommand::with_name("delete").about("Delete every dir/file founded"),
        );
    let matches = app.get_matches();
    let path = matches.value_of("path").unwrap_or_else(|| "./");

    let patterns = matches
        .values_of("patterns")
        .map(Values::collect) // or Iterator::collect
        .unwrap_or_else(|| vec![]);

    let operation = if let Some(rename) = matches.subcommand_matches("rename") {
        Operation::Rename(rename.value_of("string").unwrap_or_else(|| ""))
    } else if let Some(_) = matches.subcommand_matches("delete") {
        Operation::Delete
    } else {
        Operation::None
    };

    if let Err(e) = findx::run(Config {
        path: path,
        patterns: patterns,
        operation: operation,
    }) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}
